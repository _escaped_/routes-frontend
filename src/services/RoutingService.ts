import axios, { AxiosResponse} from 'axios';
import { LatLng } from 'leaflet';
import * as geojson from 'geojson';


export enum RouteProfile {
    POLLUTION = 'pollution',
    FASTEST = 'fastest',
}
export enum Sign {
    SLIGHTLEFT = -7,
    SHARPLEFT = -3,
    LEFT = -2,
    // SLIGHTLEFT = -1,
    STRAIGHT = 0,
    SLIGHTRIGHT = 1,
    RIGHT = 2,
    SHARPRIGHT = 3,
    DESTINATIONREACHED = 4,
    WAYPOINTREACHED = 5,
    ROUNDABOUT = 6,
    // SLIGHTRIGHT = 7,
}

type Weighting = RouteProfile.POLLUTION | RouteProfile.FASTEST;

export interface Instruction {
    distance: number;
    heading: number;
    text: string;
    time: number;
    sign: Sign;
}


export interface Route {
    distance: number;
    time: number;

    points: geojson.MultiLineString;
    instructions: Instruction[];
}


export class RoutingService {
    private baseURL: string = process.env.VUE_APP_ROUTING_URL;

    public async getRoute(origin: LatLng, destination: LatLng, profile: Weighting): Promise<Route> {
      const params = this.getQueryParams(origin, destination, profile);
      const url = `${this.baseURL}/route`;
      const response = await this.get(url, { params });
      return response.data.paths[0];
    }

    private async get(url: string, params: object): Promise<AxiosResponse> {
      params = {
        headers: { 'Content-Type': 'application/json' },
        ...params,
      };
      return await axios.get(url, params);
    }

    private getQueryParams(origin: LatLng, destination: LatLng, profile: Weighting): URLSearchParams {
        const params = new URLSearchParams();
        params.append('type', 'json');
        params.append('locale', 'en-US');
        params.append('vehicle', 'pollution');
        params.append('weighting', profile);
        params.append('elevation', 'true');
        params.append('points_encoded', 'false');
        params.append('point', origin.lat + ',' + origin.lng);
        params.append('point', destination.lat + ',' + destination.lng);

        return params;
    }
}
