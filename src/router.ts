import Vue from 'vue';
import Router from 'vue-router';

import Map from './views/Map.vue';
import Newsletter from './views/Newsletter.vue';
import Team from './views/Team.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'map',
      component: Map,
    },
    {
      path: '/newsletter',
      name: 'newsletter',
      component: Newsletter,
    },
    {
      path: '/team',
      name: 'team',
      component: Team,
    },
  ],
});
