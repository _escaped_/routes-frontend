import Vue from 'vue';

import axios, { AxiosError, AxiosResponse} from 'axios';
import vuetify from './plugins/vuetify';

import App from './App.vue';
import router from './router';
import store from './store';

Vue.config.productionTip = false;

// Fix leaflet marker icon
// see https://github.com/KoRiGaN/Vue2Leaflet/issues/103#issuecomment-346970153
import * as L from 'leaflet';
// @ts-ignore
delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});


axios.interceptors.request.use(
  (config) => {
    store.commit('SET_ERROR_MESSAGE', null);
    store.commit('IS_LOADING', true);
    return config;
  },
  (error) => {
    store.commit('IS_LOADING', false);
    return Promise.reject(error);
  },
);
axios.interceptors.response.use(
  (response) => {
    store.commit('IS_LOADING', false);
    return response;
  },
  (error) => {
    store.commit('IS_LOADING', false);
    if (!error.status) {
      store.commit(
        'SET_ERROR_MESSAGE',
        'Connection to server failed... reload to try again.',
      );
    }
    return Promise.reject(error);
  },
);



new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
