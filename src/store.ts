import Vue from 'vue';
import Vuex from 'vuex';
import { GetterTree, MutationTree, ActionTree } from 'vuex';

import { LatLng } from 'leaflet';

Vue.use(Vuex);

interface State {
  title: string;
  errorMessage: string;
  isLoading: boolean;
}

const getters: GetterTree<State, any> = {};

const mutations: MutationTree<State> = {
  CLEAR_TITLE: (state) => {
      state.title = '';
  },
  SET_TITLE: (state, title: string) => {
      state.title = title;
  },
  SET_ERROR_MESSAGE: (state, message: string) => {
      state.errorMessage = message;
  },
  IS_LOADING: (state, isLoading: boolean) => {
      state.isLoading = isLoading;
  },
};

const actions: ActionTree<State, any> = {};

const initialState: State = {
  title: '',
  errorMessage: '',
  isLoading: false,
};


export default new Vuex.Store<State>({
  state: {...initialState},
  actions,
  mutations,
  getters,
});
